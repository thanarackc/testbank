import axios from 'axios'

const fetch = async (url: any, method: any, data: any) => {
  const options = {
    method,
    headers: {
      'content-type': 'application/json',
      Token: localStorage.getItem('token')
    },
    data,
    url,
    params: method.toLocaleLowerCase() === 'get' ? { ...data } : undefined
  }
  const result = await axios(options)
  if (result.status === 203) {
    localStorage.removeItem('token')
    localStorage.removeItem('authStore')
    alert(
      'Token ของคุณหมดอายุ หรือไม่มีสิทธิ์ใช้งาน ระบบจะพาคุณกลับไปหน้า Login อีกครั้ง.'
    )
    window.location.href = process.env.PUBLIC_URL + '/'
  }
  return result
}

export default fetch
