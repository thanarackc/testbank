import fetch from './fetch'

export const saveForm = async (value: any) => {
  const url = process.env.REACT_APP_END_POINT + '/v1/app-rts/saveform'
  const method = 'POST'
  const data = value
  return await fetch(url, method, data)
}

export const formConfirm = async (value: any) => {
  const url = process.env.REACT_APP_END_POINT + '/v1/app-rts/formconfirm'
  const method = 'POST'
  const data = value
  return await fetch(url, method, data)
}

export const getForm = async (value: any) => {
  const url = process.env.REACT_APP_END_POINT + '/v1/app-rts/list'
  const method = 'POST'
  const data = value
  return await fetch(url, method, data)
}

export const getOrderInfoKerry = async (value: any) => {
  const url = process.env.REACT_APP_END_POINT + '/v1/app-rts/getOrderInfoKerry'
  const method = 'POST'
  const data = value
  return await fetch(url, method, data)
}

export const removeForm = async (id: any) => {
  const url = process.env.REACT_APP_END_POINT + '/v1/app-rts/remove/' + id
  const method = 'DELETE'
  return await fetch(url, method, undefined)
}

export const getFormById = async (id: any) => {
  const url = process.env.REACT_APP_END_POINT + '/v1/app-rts/getForm'
  const method = 'GET'
  const data = { id }
  return await fetch(url, method, data)
}

export const authLdap = async (value: any) => {
  const url = process.env.REACT_APP_END_POINT_IDAP_LOGIN
  const method = 'POST'
  const data = { username: value.username, password: value.password }
  return await fetch(url, method, data)
}

export const authApp = async (value: any) => {
  const url = process.env.REACT_APP_END_POINT + '/v1/app-rts/auth'
  const method = 'POST'
  const data = { username: value.username }
  return await fetch(url, method, data)
}

export const checkEmp = async (value: any) => {
  const url = process.env.REACT_APP_END_POINT + '/v1/app-rts/checkemp'
  const method = 'POST'
  const data = { empId: value }
  return await fetch(url, method, data)
}

export const getOrderNumberList = async (value: any) => {
  const url = process.env.REACT_APP_END_POINT + '/v1/app-rts/list-number'
  const method = 'POST'
  const data = value
  return await fetch(url, method, data)
}

export const getOrderNumber = async (value: any) => {
  const url = process.env.REACT_APP_END_POINT + '/v1/app-rts/get-order-number'
  const method = 'POST'
  const data = value
  return await fetch(url, method, data)
}

export const addNumber = async (value: any) => {
  const url = process.env.REACT_APP_END_POINT + '/v1/app-rts/add-number'
  const method = 'POST'
  const data = value
  return await fetch(url, method, data)
}

export const onSelectToOrderBox = async (value: any) => {
  const url = process.env.REACT_APP_END_POINT + '/v1/app-rts/selection-order-box'
  const method = 'POST'
  const data = value
  return await fetch(url, method, data)
}

export const onGetItem = async (value: any) => {
  const url = process.env.REACT_APP_END_POINT + '/v1/app-rts/getitem'
  const method = 'POST'
  const data = value
  return await fetch(url, method, data)
}

export const onBoxTransaction = async (value: any) => {
  const url = process.env.REACT_APP_END_POINT + '/v1/app-rts/box-update'
  const method = 'POST'
  const data = value
  return await fetch(url, method, data)
}

export const onOpenBox = async (value: any) => {
  const url = process.env.REACT_APP_END_POINT + '/v1/app-rts/box-open'
  const method = 'POST'
  const data = value
  return await fetch(url, method, data)
}

export const onGetLogHistory = async (value: any) => {
  const url = process.env.REACT_APP_END_POINT + '/v1/app-rts/box-log-list'
  const method = 'POST'
  const data = value
  return await fetch(url, method, data)
}

export const onPrintForm = async (value: any) => {
  const url = process.env.REACT_APP_END_POINT + '/v1/app-rts/printform'
  const method = 'POST'
  const data = value
  return await fetch(url, method, data)
}

export const onPrintFormAccount = async (value: any) => {
  const url = process.env.REACT_APP_END_POINT + '/v1/app-rts/printformaccount'
  const method = 'POST'
  const data = value
  return await fetch(url, method, data)
}

export const onExportCsvFile = async (value: any) => {
  const url = process.env.REACT_APP_END_POINT + '/v1/app-rts/exportcsvfile'
  const method = 'POST'
  const data = value
  return await fetch(url, method, data)
}

export const onPrintFormBox = async (value: any) => {
  const url = process.env.REACT_APP_END_POINT + '/v1/app-rts/printformbox'
  const method = 'POST'
  const data = value
  return await fetch(url, method, data)
}