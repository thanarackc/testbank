import React from 'react'
import {
  Row,
  Col,
  Card,
  Typography,
  Form,
  Input,
  Icon,
  Button,
  message,
} from 'antd'
import { observer, inject } from 'mobx-react'
import { authLdap, authApp } from '../../actions/order'

const { Title } = Typography

@inject('authStore')
@observer
class Login extends React.Component<any, any> {
  usernameinput: any = React.createRef()

  state = {
    loginDisabled: false,
  }

  constructor(props: any) {
    super(props)
  }

  async componentDidMount() {
    // console.log(this.props)
    this.usernameinput.current.focus()
  }

  handleSubmit = (e: any) => {
    e.preventDefault()
    // window.location.href = process.env.PUBLIC_URL + '/'
    this.props.history.push('/')
  }

  headLogin = () => {
    return (
      <React.Fragment>
        <div className="sign-text">
          <div>Singin</div>
        </div>
      </React.Fragment>
    )
  }

  render() {
    const { getFieldDecorator } = this.props.form
    const { loginDisabled } = this.state
    return (
      <React.Fragment>
        <div
          className="login-page"
          style={{
            background: `url(${process.env.PUBLIC_URL + '/bglogin.jpg'})`,
          }}
        >
          <Row
            className="box"
            type="flex"
            justify="space-around"
            align="middle"
          >
            <Col span={6}>
              <Card title={this.headLogin()}>
                <Form onSubmit={this.handleSubmit} className="login-form">
                  <Form.Item label="Username">
                    {getFieldDecorator('username', {
                      rules: [
                        {
                          required: true,
                          message: 'กรุณากรอก Username',
                        },
                      ],
                    })(
                      <Input
                        ref={this.usernameinput}
                        autoComplete="off"
                        type="email"
                        prefix={
                          <Icon
                            type="user"
                            style={{ color: 'rgba(0,0,0,.25)' }}
                          />
                        }
                        placeholder="Username"
                      />
                    )}
                  </Form.Item>
                  <Form.Item label="Password">
                    {getFieldDecorator('password', {
                      rules: [
                        {
                          required: true,
                          message: 'กรุณากรอก Password',
                        },
                      ],
                    })(
                      <Input
                        autoComplete="off"
                        prefix={
                          <Icon
                            type="lock"
                            style={{ color: 'rgba(0,0,0,.25)' }}
                          />
                        }
                        type="password"
                        placeholder="Password"
                      />
                    )}
                  </Form.Item>
                  <Form.Item>
                    <div>
                      <Button
                        loading={loginDisabled}
                        disabled={loginDisabled}
                        type="primary"
                        htmlType="submit"
                        icon="user"
                        className="login-form-button"
                      >
                        SING IN
                      </Button>
                    </div>
                    <div className="forget-password">Forget Password ?</div>
                  </Form.Item>
                  <Form.Item label="New user">
                    <div>
                      <Button
                        loading={loginDisabled}
                        disabled={loginDisabled}
                        type="primary"
                        htmlType="submit"
                        icon="user"
                        className="login-form-button"
                      >
                        SING UP
                      </Button>
                    </div>
                  </Form.Item>
                </Form>
              </Card>
            </Col>
          </Row>
        </div>
      </React.Fragment>
    )
  }
}

const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(Login)
export default WrappedNormalLoginForm
