import React, { useState, useEffect } from 'react'

import {
  Layout,
  Menu,
  Breadcrumb,
  Card,
  Row,
  Col,
  Form,
  Input,
  Icon,
  Button,
  Rate,
} from 'antd'
const { Header, Content, Footer } = Layout

const { Meta } = Card

const Dashboard = () => {
  const [posts, setPosts] = useState([
    {
      title: 'ทดสอบ',
      desc: 'ทดสอบทดสอบ',
      rate: 5,
    },
    {
      title: 'ทดสอบ2',
      desc: 'ทดสอบทดสอบ2',
      rate: 3,
    },
  ])
  const [title, setTitle] = useState('')
  const [desc, setDesc] = useState('')
  const [rate, setRate] = useState('')

  useEffect(() => {
    //
  }, [posts])

  const addPost = () => {
    const setData: any = [
      {
        title: title,
        desc: desc,
        rate: rate,
      },
    ]
    const data = posts.concat(setData)
    setPosts(data)
  }

  return (
    <div id="components-layout-demo-top">
      <Layout className="layout">
        <Header>
          <div className="logo" />
          <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['2']}>
            <Menu.Item key="1">File</Menu.Item>
            <Menu.Item key="2">Edit</Menu.Item>
            <Menu.Item key="3">Option</Menu.Item>
            <Menu.Item key="4">Tool</Menu.Item>
            <Menu.Item key="5">Window</Menu.Item>
            <Menu.Item key="6">Help</Menu.Item>
          </Menu>
        </Header>
        <Content style={{ padding: '0 50px' }}>
          <div className="site-layout-content">
            <Row>
              <Col span={12}>
                {posts.map((val: any, index) => (
                  <Col span={6}>
                    <Card
                      key={index}
                      hoverable
                      style={{ width: '100%' }}
                      cover={
                        <img
                          alt="example"
                          src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png"
                        />
                      }
                    >
                      <Meta title={val.title} description={val.desc} />
                      <Rate disabled defaultValue={val.rate} />
                    </Card>
                  </Col>
                ))}
              </Col>
              <Col span={12}>
                <Form>
                  <Form.Item label="Title">
                    <Input
                      autoComplete="off"
                      placeholder="Title"
                      onChange={(e) => setTitle(e.target.value)}
                    />
                  </Form.Item>
                  <Form.Item label="Desciption">
                    <Input
                      autoComplete="off"
                      placeholder="Desciption"
                      onChange={(e) => setDesc(e.target.value)}
                    />
                  </Form.Item>
                  <Form.Item label="Rate">
                    <Input
                      autoComplete="off"
                      placeholder="Rate"
                      onChange={(e) => setRate(e.target.value)}
                    />
                  </Form.Item>
                  <Form.Item className="text-center">
                    <Button onClick={addPost}>Add</Button>
                  </Form.Item>
                </Form>
              </Col>
            </Row>
          </div>
        </Content>
        <Footer style={{ textAlign: 'center' }}>
          Ant Design ©2018 Created by Ant UED
        </Footer>
      </Layout>
    </div>
  )
}

export default Dashboard
