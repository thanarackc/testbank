import React from 'react'
import Dashboard from './views/dashboard'
import Login from './views/login'
import { Route, Switch } from 'react-router-dom'
import PrivateRoute from './components/layout/privateRoute'
import 'antd/dist/antd.css'
import './App.scss'

function App() {
  return (
    <div className="App">
      <Switch>
        {/* <PrivateRoute exact path="/" component={Dashboard} /> */}
        <Route exact path="/" component={Dashboard} />
        <Route exact path="/login" component={Login} />
      </Switch>
    </div>
  )
}

export default App
