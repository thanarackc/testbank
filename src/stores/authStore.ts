import { observable, action } from 'mobx'
import { persist } from 'mobx-persist'
import * as jwt from 'jsonwebtoken'

// Mock user infomation.
const userInfo = {}

export class AuthStore {
  @persist('object') @observable isAuth = false
  @persist('object') @observable token = ''
  @persist('object') @observable userInfo = userInfo

  @action
  public setAuth(status: boolean): void {
    this.isAuth = status
  }

  @action
  public setToken(
    token: string,
    role: string,
    isApprove = 0
  ): void {
    const userInfo: any = jwt.verify(token, 'watson-jwt')
    localStorage.setItem('token', token)
    this.token = token
    this.userInfo = { ...userInfo, appRole: role, isApprove }
  }

  @action
  public logout(): void {
    localStorage.removeItem('token')
    localStorage.removeItem('authStore')
  }
}
