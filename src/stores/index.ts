import { LayoutStore } from './layoutStore'
import { create } from 'mobx-persist'
import { AuthStore } from './authStore'

interface Stores {
  [key: string]: any
}

export const stores: Stores = {
  layoutStore: new LayoutStore(),
  authStore: new AuthStore()
}

const hydrate = create({
  storage: localStorage,
  jsonify: true
})

Object.keys(stores).map(val => hydrate(val, stores[val]))
