export const onCheckRole = (group = 'A', role = '', userInfo: any = {}) => {
  // const role = this.props.authStore.userInfo.appRole
  // A = Can create new form
  if (group === 'A') {
    return ['store', 'wh', 'whs', 'admin'].includes(role)
  }
  // B = Wh group
  if (group === 'B') {
    return ['wh', 'admin'].includes(role)
  }
  // C = Cs group
  if (group === 'C') {
    return ['cs', 'admin'].includes(role)
  }
  // D = Ecom group
  if (group === 'D') {
    return ['ecom', 'admin'].includes(role)
  }
  // E = Store group
  if (group === 'E') {
    return ['store', 'admin'].includes(role)
  }
  // F = Ecom Cs Wh group
  if (group === 'F') {
    return ['ecom', 'cs', 'wh', 'admin'].includes(role)
  }
  // G = Ecom cs group
  if (group === 'G') {
    return ['ecom', 'cs', 'admin'].includes(role)
  }
  // H = Ac group
  if (group === 'H') {
    return ['ac', 'admin'].includes(role)
  }
  if (group === 'ADD_TO_BOX_HOME') {
    return ['store', 'admin'].includes(role)
  }
  if (group === 'EXPORT_CSV') {
    return ['cs', 'admin'].includes(role)
  }
  if (group === 'EXPORT_CSV_FAST') {
    return ['cs', 'whs', 'wh', 'ecom', 'store', 'it', 'ac', 'admin'].includes(
      role
    )
  }
  if (group === 'OPEN_ORDER_WHS') {
    return ['cs', 'admin'].includes(role)
  }
  if (group === 'EXPORT_PRINT_AC') {
    return ['ac', 'ecom', 'cs', 'admin'].includes(role)
  }
  if (group === 'CONFIRM_STEP_1') {
    return ['ecom', 'admin'].includes(role) && userInfo.isApprove === 1
  }
  if (group === 'IT') {
    return ['it', 'admin'].includes(role)
  }
  // Admin
  if (group === 'ADMIN') {
    return ['admin'].includes(role)
  }
  if (group === '') {
    return true
  }
  return false
}
