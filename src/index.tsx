import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import * as serviceWorker from './serviceWorker'
import { BrowserRouter as Router } from 'react-router-dom'
// import { createBrowserHistory } from "history"
import { stores } from './stores'
import { Provider } from 'mobx-react'
import frFR from 'antd/es/locale/th_TH'
import { ConfigProvider } from 'antd'
import * as moment from 'moment'
import './index.css'

moment.locale('th')

// const history = createBrowserHistory()

ReactDOM.render(
  <Provider {...stores}>
    <Router basename={process.env.PUBLIC_URL}>
      <ConfigProvider locale={frFR}>
        <App />
      </ConfigProvider>
    </Router>
  </Provider>,
  document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
