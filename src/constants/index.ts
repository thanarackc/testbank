const createMonth = Array.from(Array(31).keys())
const createHoute = Array.from(Array(24).keys())
const createYear = Array.from(Array(12).keys())

export const DATEFORMAT = 'YYYY-MM-DD'
export const DATETIMEFORMAT = 'YYYY-MM-DD HH:mm:ss'
export const DATETIMEFORMATHUMAN = 'DD/MM/YYYY HH:mm:ss'
export const labelMonth = createMonth.map(value => (value += 1))
export const labelYear = createYear.map(value => (value += 1))
export const labelHour = createHoute.map(value => value.toFixed(2))
export const labelWeek = [
  'Mon.',
  'Tue.',
  'Wed.',
  'Thu.',
  'Fri.',
  'Sat.',
  'Sun.'
]
export const status = [
  // { id: 0, name: '--กรุณาเลือกเหตุผล--' },
  { id: 1, name: 'เสียหาย' },
  { id: 2, name: 'สินค้าที่ได้รับไม่ถูกต้อง' },
  { id: 3, name: 'สินค้าหมดอายุ/ใกล้หมดอายุ' },
  { id: 4, name: 'ชำรุดบกพร่องจากโรงงาน' },
  {
    id: 5,
    name: 'Over Selling',
    role: ['admin', 'whs', 'ecom', 'cs', 'wh', 'it', 'ac']
  },
  {
    id: 6,
    name: 'Refund from over selling',
    role: ['admin', 'whs', 'ecom', 'cs', 'wh', 'it', 'ac']
  },
  { id: 7, name: 'อื่นๆ', more: true }
]

export const statusRefund = [
  { id: 'Open', name: 'เปิดรายการรับคืนสินค้า' },
  { id: 'SendToWH', name: 'ส่งสินค้าเข้าคลัง' },
  { id: 'WHReceiving', name: 'คลังรับสินค้า' },
  { id: 'CSCockpitApproved', name: 'อนุมัติในระบบ CSCockpit' },
  { id: 'Csconfirm', name: 'CS อนุมัติ' },
  { id: 'eComApproved', name: 'Ecom อนุมัติ' }
]
