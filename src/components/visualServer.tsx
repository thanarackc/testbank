import React from "react"
import { Row, Col } from "reactstrap"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import {
  faArrowRight,
  faServer,
  faTimes
} from "@fortawesome/free-solid-svg-icons"

export const VisualServer = (props: any) => {
  let server = [
    { name: "Kerry", status: "active" },
    { name: "Wins", status: "active" },
    { name: "Hybris", status: "active" }
  ]
  if (props.status === "waiting") {
    server[2].status = "unactive"
  }
  if (props.status === "shipped") {
    server[2].status = "unactive"
  }
  if (props.status === "confirm") {
    server[2].status = "unactive"
  }
  if (props.status === "complete") {
    server[1].status = "unactive"
    // server[2].status = "unactive"
  }
  return (
    <div className="visual">
      <Row className="text-center">
        <Col md={{ size: 3 }} className="mb-3">
          <div className={`server ${server[0].status}`}>
            <p>
              <FontAwesomeIcon icon={faServer} className="server-icon" />
            </p>
            <h6>{server[0].name}</h6>
          </div>
        </Col>
        <Col md={{ size: 1 }} sm="12" className="mb-3">
          <div className={`a-r ${server[1].status}`}>
            {server[1].status === "active" && (
              <FontAwesomeIcon icon={faArrowRight} />
            )}
            {server[1].status === "unactive" && (
              <FontAwesomeIcon icon={faTimes} />
            )}
          </div>
        </Col>
        <Col md={{ size: 3 }} className="mb-3">
          <div className={`server ${server[1].status}`}>
            <p>
              <FontAwesomeIcon icon={faServer} className="server-icon" />
            </p>
            <h6>Wins</h6>
          </div>
        </Col>
        <Col md={{ size: 1 }} sm="12" className="mb-3">
          <div className={`a-r ${server[2].status}`}>
            {server[2].status === "active" && (
              <FontAwesomeIcon icon={faArrowRight} />
            )}
            {server[2].status === "unactive" && (
              <FontAwesomeIcon icon={faTimes} />
            )}
          </div>
        </Col>
        <Col md={{ size: 3 }} className="mb-3">
          <div className={`server ${server[2].status}`}>
            <p>
              <FontAwesomeIcon icon={faServer} className="server-icon" />
            </p>
            <h6>Hybris</h6>
          </div>
        </Col>
      </Row>
    </div>
  )
}
