import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import { inject, observer } from 'mobx-react'

const storage: string = localStorage.getItem('authStore') || '{}'
const authStore = JSON.parse(storage)

const PrivateRoute = inject('authStore')(
  observer(({ component: Component, ...rest }: any) => {
    // console.log('xxxxxxx', rest)
    return (
      <Route
        {...rest}
        render={props =>
          authStore.token && authStore.userInfo ? (
            <Component {...props} userInfo={authStore.userInfo} />
          ) : (
            <Redirect to="/login" />
          )
        }
      />
    )
  })
)

export default PrivateRoute
