import React from 'react'
import { Layout, Menu, Breadcrumb, Dropdown, Icon, Modal } from 'antd'
import { observer, inject } from 'mobx-react'
import { Link } from 'react-router-dom'
const { Header, Content, Footer } = Layout

@inject('layoutStore', 'authStore')
@observer
class MainLayout extends React.Component<any, any> {
  state = {
    defaultSelectedKeys: ['1'],
    logoutShow: false
  }

  constructor(props: any) {
    super(props)
    console.log(props)
  }

  async componentDidMount() {
    this.onActiveMenu()
  }

  onActiveMenu = () => {
    let menuActive = ['1']
    const path = this.props.location.pathname
    const pathSp = path.split('/')
    if (path === '/') {
      menuActive = ['1']
    }
    this.setState({
      defaultSelectedKeys: menuActive
    })
  }

  onMenuItem = (item: any) => {
    if (item.key === '1') {
      this.props.history.push('/')
    }
  }

  onLogout = () => {
    this.props.authStore.logout()
    window.location.href = process.env.PUBLIC_URL + '/'
  }

  onCancelLogout = () => {
    this.setState({ logoutShow: false })
  }

  onShowLogout = () => {
    this.setState({ logoutShow: true })
  }

  render() {
    // const bc = this.props.bc || []
    const isRender = this.props.isRender
    const { userInfo } = this.props.authStore
    const menu = (
      <Menu>
        <Menu.Item key="login-menu" onClick={this.onShowLogout}>
          ออกจากระบบ
        </Menu.Item>
      </Menu>
    )

    return (
      <React.Fragment>
        <Modal
          title="ยืนยัน"
          visible={this.state.logoutShow}
          onOk={this.onLogout}
          onCancel={this.onCancelLogout}
        >
          <p>คุณต้องการออกจากระบบใช่หรือไม่ ?</p>
        </Modal>
        <Layout className="layout">
          <Header className="header">
            <Link to="/">
              <div className="logo">
                <img src={process.env.PUBLIC_URL + '/logo-watsons-.png'} />
              </div>
            </Link>
            <Menu
              theme="dark"
              mode="horizontal"
              // defaultSelectedKeys={this.state.defaultSelectedKeys}
              selectedKeys={this.state.defaultSelectedKeys}
              style={{ lineHeight: '64px' }}
              onClick={this.onMenuItem}
            >
              <Menu.Item key="1">เพิ่มใบกำกับภาษี</Menu.Item>
            </Menu>
            <div className="right-head">
              <Dropdown overlay={menu} trigger={['click']}>
                <div className="name-position">
                  <a className="ant-dropdown-link" href="#">
                    <span>{userInfo.userDisplayName || userInfo.name}</span>
                    <span className="app-role-text">{userInfo.appRole}</span>
                    <Icon type="down" />
                  </a>
                </div>
              </Dropdown>
            </div>
          </Header>
          <Content
            style={{
              padding: '0 50px'
            }}
          >
            {/* <Breadcrumb style={{ margin: '16px 0' }}>
              <Breadcrumb.Item>หน้าหลัก</Breadcrumb.Item>
              {bc.map((v: any) => (
                <Breadcrumb.Item key={v.name}>{v.name}</Breadcrumb.Item>
              ))}
            </Breadcrumb> */}
            <div
              style={{
                background: '#fff',
                padding: 24,
                minHeight: 280,
                margin: '16px 0'
              }}
            >
              {isRender ? this.props.children : ''}
            </div>
          </Content>
          <Footer style={{ textAlign: 'center' }}>
            Ant Design ©2020 Created by Watsons IT. Environment {process.env.NODE_ENV}
          </Footer>
        </Layout>
      </React.Fragment>
    )
  }
}

export default MainLayout
